<?php
// vim: ts=4 foldcolumn=4 foldmethod=marker
/**
 * DP_User class found here.
 * 
 * This file is part of Reblog,
 * a derivative work of Feed On Feeds.
 *
 * Distributed under the Gnu Public License.
 *
 * @package Refeed
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author Michal Migurski <mike@stamen.com>
 * @author Michael Frumin <mfrumin@eyebeam.org>
 * @author Luiz Fernando Ramos Lemos <kawazs@gmail.com>
 * @copyright �2004 Michael Frumin, Michal Migurski
 * @link http://reblog.org Reblog
 * @link http://feedonfeeds.com Feed On Feeds
 * @link http://drupal.org/project/reblog Drupal Reblog integration module
 * @version $Revision$
 */
 
   /**
    * DP_User is an authentication class for authenticating with the Drupal cookie
    *
    * note that it authenticates for free in the /out directory, for public viewing of the
    * output items/feeds
    */
    class DP_User extends RF_User
    {

        var $name = '';

        function _authenticate(&$dbh)
        {


            if(basename(dirname($_SERVER['SCRIPT_NAME'])) == "out") {
                if(empty($_GET['user'])) {
                    $_GET['user'] = 1;
                }

                $name = $dbh->getOne("SELECT name FROM users WHERE uid = ?", array($_GET['user']));
                $user = new DP_User(array('id' => $_GET['user'], 'name' => $name));
                return $user;
            }

            foreach($_COOKIE as $v)
                if($dbh->fetchInto($dbh->simpleQuery("SELECT uid FROM sessions WHERE sid = '$v' "),$session,null)) break;

            /*Role based auth*/
            $dbh->fetchInto($dbh->simpleQuery("SELECT rid FROM permission WHERE perm = 'reblog' "),$roles,null);
            foreach( $roles as $v)
                $perm = $dbh->getOne("SELECT uid FROM users_roles WHERE uid = '$session[0]' AND rid = '$v' ");

            if( $session[0] == $perm ) {
                $row = $dbh->getRow("SELECT * FROM users WHERE uid = ?", array($session[0]), DB_FETCHMODE_ASSOC);
                if(!DB::isError($row)) {
                    if(!empty($row['uid']) && $row['status']) {
                        $user = new DP_User(array('id' => $row['uid'], 'name' => $row['name']));

                        return $user;
                    }
                }
            }
            header(sprintf("Location: ../user/login?destination=refeed"));
        }

        function DP_User($args)
        {
            $this->name = empty($args['name']) ? $this->id : $args['name'];

            parent::RF_User($args);
        }

        function selfName()
        {

            return $this->name;
        }


    }

?>
